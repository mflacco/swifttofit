//
//  SizeToFitTVC.swift
//  SwiftToFit
//
//  Created by Michael on 6/14/14.
//  Copyright (c) 2014 FlaccoDev. All rights reserved.
//

import UIKit

//NO need to import classes in project

class SizeToFitTVC: UITableViewController {

     var textArray=String[]()
    
//using this seemed to cause trouble
//    init(style: UITableViewStyle) {
//        super.init(style: style)
//       
//
//        
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let textString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat nunc a erat faucibus, sit amet interdum ipsum auctor. Praesent sagittis feugiat aliquet. Phasellus dictum laoreet auctor. Nulla aliquet enim ut ligula sodales, sed tempor nisl interdum. Nullam et libero elit. In dignissim dignissim pharetra. Proin in ligula a erat vehicula mollis."
        let fullTextArray=textString.componentsSeparatedByString(" ")
        var labelString=""
        
        // if you want to get the index/value tuple you need "enumerate"
        for (index, value) in enumerate(fullTextArray){
            
            if index > 0 {labelString += " "}
            labelString += fullTextArray[index]
            self.textArray += labelString;
            
        }
        // i don't know why this weird .self construction is needed but it is
        tableView.registerClass(STFTableViewCell.self, forCellReuseIdentifier: "STFCell")
        tableView.estimatedRowHeight=44.0
        tableView.rowHeight=UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//     #pragma mark - Table view data source



    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return textArray.count
    }
    
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let celldentifier = "STFCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(celldentifier, forIndexPath: indexPath) as STFTableViewCell
        cell.cellLabel.text=textArray[indexPath.row]
        
        return cell
        
    
    }

 
}
